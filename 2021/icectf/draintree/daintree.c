#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define CART_SIZE 8
#define ARRAY_LENGTH(array) (sizeof((array)) / sizeof((array)[0]))

struct item {
    const char *name;
    int price;
};

static const struct item avail_items[] = {
    {"Wireless speaker", 100},
    {"Batteries", 10},
    {"Gaming console with chicken warmer", 2000},
    {"Wrench", 5},
    {"Webcam", 30},
};

void setup(void)
{
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

void give_flag(void)
{
    char buf[128];

    puts("Congratulations! Here is a flag.");

    int fd = open("flag.txt", O_RDONLY);
    if (fd < 0) {
        puts("There was a problem, please tell an admin :(");
        _exit(1);
    }

    ssize_t l = read(fd, buf, sizeof(buf));
    if (l < 0) {
        puts("There was a problem, please tell an admin :(");
        _exit(1);
    }

    write(1, buf, l);
    close(fd);
}

void print_menu(void)
{
    puts("\nMenu:");
    puts("[s]how available items");
    puts("[a]dd item to cart");
    puts("show [c]art");
    puts("check [o]ut");
    puts("enter a [d]iscount code");
    puts("[l]eave");
    printf("your choice> ");
}

int read_int(void)
{
    char buf[10];

    if (fgets(buf, sizeof(buf), stdin) == NULL) {
        return 0;
    }

    return atoi(buf);
}

int main(void)
{
    char choice[2] = {0};
    char buf[32];
    bool done = false;
    bool discount = false;
    struct item cart[CART_SIZE] = {0};
    int items_in_cart = 0;

    setup();

    puts("Welcome to our store! Feel free to have a look!");

    while (!done) {
        print_menu();
        read(0, choice, sizeof(choice));

        switch(choice[0]) {

        case 's':
            for (int i = 0; i < ARRAY_LENGTH(avail_items); i++) {
                printf("[%d] %s: %d €\n", i, avail_items[i].name, avail_items[i].price);
            }
            break;

        case 'a': {
            printf("Which item #? ");
            int i = read_int();
            if (i < 0 || i >= ARRAY_LENGTH(avail_items)) {
                puts("Nope");
            } else if (items_in_cart >= CART_SIZE) {
                printf("Sorry, your cart can't have more than %d items\n", CART_SIZE);
            } else {
                cart[items_in_cart++] = avail_items[i];
            }
        } break;

        case 'c':
            for (int i = 0; i < items_in_cart; i++) {
                printf("[%d] %s: %d €\n", i, cart[i].name, cart[i].price);
            }
            break;

        case 'o': {
            int total = 0;
            for (int i = 0; i < items_in_cart; i++) {
                total += cart[i].price;
            }

            if (discount) {
                total *= 0.8f;
            }

            printf("Total: %d €. Thanks for your purchase!\n", total);
            items_in_cart = 0;
            discount = false;
        } break;

        case 'd': {
            printf("Length of the code: ");
            int length = read_int();
            printf("Code: ");
            ssize_t read_ret = read(0, buf, length);
            if (read_ret < 0) {
                puts("Something is wrong, tell an admin!");
                _exit(0);
            }

            if (read_ret > 0 && buf[read_ret - 1] == '\n') {
                buf[read_ret - 1] = '\0';
            }

            if (strcmp(buf, "1337AA4919") == 0) {
                puts("20% discount applied!");
                discount = true;
            } else {
                puts("Hmm, that doesn't look right...");
            }
        } break;

        case 'x':
            printf("Debug: %p\n", give_flag);
            break;

        case 'l':
            puts("Bye!");
            done = true;
            break;

        default:
            puts("Invalid choice");
            break;
        }
    }

    return 0;
}
