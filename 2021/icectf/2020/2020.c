#include <stdio.h>
#include <stdarg.h>
#include <string.h>


extern char *mysnprintf(char *buf, size_t len, const char *format, ...);


void event(char *input)
{
	char buf[300];
	memset (buf, 0, sizeof(buf));
	mysnprintf (buf, sizeof(buf), input);
	printf ("Oh yeah? Let me echo that right back at you.\n%s", buf);
	fflush (stdout);
}


/* This is where 2020 happens */
void event_loop(void)
{
	char inp[300];

	while (fgets(inp, sizeof(inp), stdin) != NULL)
	{
		event (inp);
	}
}

int main(int argc, char **argv)
{
	event_loop();

	return 0;
}

