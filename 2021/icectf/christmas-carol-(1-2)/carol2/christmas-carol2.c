#include <fcntl.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define ARRAY_LENGTH(array) (sizeof((array)) / sizeof((array)[0]))

void setup(void)
{
    setvbuf(stdin, NULL, _IONBF, 0);
    setvbuf(stdout, NULL, _IONBF, 0);
    setvbuf(stderr, NULL, _IONBF, 0);
}

void print_menu(void)
{
    puts("\nMenu:");
    puts("[a]dd a Christmas carol");
    puts("[s]how a Christmas carol");
    puts("[d]delete a Christmas carol");
    puts("[e]xit");
    printf("your choice> ");
}

int read_int(void)
{
    char buf[10];

    if (fgets(buf, sizeof(buf), stdin) == NULL) {
        return 0;
    }

    return atoi(buf);
}

void read_string(char *buf, size_t size)
{
    ssize_t read_ret = read(0, buf, size);
    if (read_ret < 0) {
        puts("Something is wrong!");
        _exit(0);
    }

    if (read_ret > 0 && buf[read_ret - 1] == '\n') {
        buf[read_ret - 1] = '\0';
    }
}

int main(void)
{
    char choice[2] = {0};
    char *carols[16] = {0};
    int num_carols = 0;
    bool done = false;

    puts("HO! HO! HO! Merry Christmas! I've heard that Santa is going to bring"
        " flags this year...\n");

    setup();

    while (!done) {
        print_menu();
        read(0, choice, sizeof(choice));

        switch(choice[0]) {
        case 'a': {
            if (num_carols >= ARRAY_LENGTH(carols)) {
                puts("That's a bit too many carols");
            } else {
                printf("How long is it? ");
                int length = read_int();
                char *carol = malloc(length);
                printf("Enter the lyrics ");
                read_string(carol, length);
                carols[num_carols++] = carol;
            }
        } break;

        case 's': {
            printf("Which carol would you like to show? ");
            int i = read_int();
            if (i < 0 || i >= ARRAY_LENGTH(carols)) {
                puts("Nope");
            } else {
                puts(carols[i]);
            }
        } break;

        case 'd': {
            printf("Which carol would you like to delete? ");
            int i = read_int();
            if (i < 0 || i >= ARRAY_LENGTH(carols)) {
                puts("Nope");
            } else {
                free(carols[i]);
                num_carols--;
            }
        } break;

        case 'x': {
            void *saved_rip_addr = __builtin_frame_address(0) + 8;
            printf("I've found this in my chimney, do you know what it is?: %p\n", saved_rip_addr);
        } break;

        case 'e':
            puts("See you next Christmas!");
            done = true;
            break;

        default:
            puts("Invalid choice");
            break;
        }
    }

    return 0;
}
