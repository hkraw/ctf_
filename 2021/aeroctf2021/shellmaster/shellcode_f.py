from capstone import *

d = [0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90, 0x90]
    


result = []
md = Cs(CS_ARCH_X86, CS_MODE_64)


for ii in range(0,256):
    for jj in range(0,256):
            CODE = bytes([jj,ii]) + bytes(d)
            
            for (address, size, mnemonic, op_str) in md.disasm_lite(CODE, 0 + 0x200):
                    if mnemonic == "nop":
                        break

                    r = "%s %s" %(mnemonic, op_str)

                    result.append(r)
result = list(set(result))
result = sorted(result)

f = open("asm_list.txt","w")
f.write("\n".join(result))
f.close()
